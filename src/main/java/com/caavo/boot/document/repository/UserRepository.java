package com.caavo.boot.document.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.caavo.boot.document.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	User findByUserName(String userName);
	User findByMailId(String mailId);
	User findByuserNameAndPassword(String username, String password);
	User findByUserId(int id);
}
