package com.caavo.boot.document.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.caavo.boot.document.entity.Details;

public interface CaavoRepository extends JpaRepository<Details, Integer>{
	Details getByArtifactNumber(String artifactNumber);
	List<Details> getByUserId(int userId);
	
}
