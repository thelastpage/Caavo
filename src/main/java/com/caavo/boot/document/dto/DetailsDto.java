package com.caavo.boot.document.dto;

import java.util.Date;

public class DetailsDto {
	
	private int id;
	private String idType;
	private String  idNumber;
	private Date issueDate;
	private String action="Delete";
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public String getAction() {
		return action;
	}
	
}