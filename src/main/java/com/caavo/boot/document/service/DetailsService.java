package com.caavo.boot.document.service;

import java.util.List;

import com.caavo.boot.document.dto.DetailsDto;
import com.caavo.boot.document.entity.Details;
import com.caavo.boot.document.entity.User;
import com.caavo.boot.document.exception.ServiceException;

public interface DetailsService {

	void insert(Details detailsDto)throws ServiceException;

	List<DetailsDto> fetchDetails(int userId)throws ServiceException;

	User login(User user)throws ServiceException;

	void signUp(User user)throws ServiceException;

	User getUser(int userId)throws ServiceException;

	User updateUser(int userId, User user)throws ServiceException;

	List<Details> fetchLockerDetails(int userId)throws ServiceException;
	
	public String setJwtHeader(User user)throws ServiceException;
	
	public String getJwtHeader(String token)throws ServiceException;

	void deleteDetails(int id)throws ServiceException;;

}
