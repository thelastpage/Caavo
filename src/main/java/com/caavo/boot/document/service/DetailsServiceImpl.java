package com.caavo.boot.document.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.caavo.boot.document.dto.DetailsDto;
import com.caavo.boot.document.entity.Details;
import com.caavo.boot.document.entity.User;
import com.caavo.boot.document.exception.ServiceException;
import com.caavo.boot.document.repository.CaavoRepository;
import com.caavo.boot.document.repository.UserRepository;

@Service
public class DetailsServiceImpl implements DetailsService {
	
	@Autowired
	CaavoRepository caavoRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	JWTAuth jwtAuth;
	
	@Override
	public void insert(Details detailsDto) throws ServiceException{
		Details temp= caavoRepository.getByArtifactNumber(detailsDto.getArtifactNumber());
		if(null==temp)
			caavoRepository.save(detailsDto);		
		else {
			detailsDto.setId(temp.getId());
			detailsDto=caavoRepository.saveAndFlush(detailsDto);			
		}
	}

	@Override
	public List<DetailsDto> fetchDetails(int userId)throws ServiceException {
		// TODO Auto-generated method stub
		
		List<Details> details= caavoRepository.getByUserId(userId);
		List<DetailsDto> detailsDto=new ArrayList<>();
		for(Details detail:details) {
			DetailsDto dto=new DetailsDto();
			dto.setId(detail.getId());
			dto.setIdNumber(detail.getArtifactNumber());
			dto.setIdType(detail.getDetailsType().toUpperCase());
			dto.setIssueDate(detail.getDob());
			
			detailsDto.add(dto);
		}
		
		return detailsDto;
	}

	@Override
	public User login(User user)throws ServiceException {
		User bot= userRepository.findByuserNameAndPassword(user.getUserName(), user.getPassword());
		if(null!=bot)
			bot.setPassword("");
		return bot;
	}
	
	@Override
	public String setJwtHeader(User user)throws ServiceException {
		return jwtAuth.createJWT(String.valueOf(user.getUserId()), "Application", String.valueOf(user.getUserName()), 1222000);
	}
	
	@Override
	public String getJwtHeader(String token)throws ServiceException {
		return jwtAuth.parseJWT(token);
	}

	@Override
	public void signUp(User user) throws ServiceException{
		if(null==userRepository.findByUserName(user.getUserName()))
			userRepository.saveAndFlush(user);	
	}

	@Override
	public User getUser(int user) throws ServiceException{
		User userDetails= userRepository.findByUserId(user);
		userDetails.setDetails(caavoRepository.getByUserId(user));
		return userDetails;
	}

	@Override
	public User updateUser(int userId,User user)throws ServiceException {
		// TODO Auto-generated method stub
		user.setUserId(userId);
		User details= userRepository.saveAndFlush(user);
		details.setDetails(caavoRepository.getByUserId(userId));
		return details;
	}

	@Override
	public List<Details> fetchLockerDetails(int userId)throws ServiceException {
		List<Details> list= caavoRepository.getByUserId(userId);
		return list;
	}

	@Override
	public void deleteDetails(int id) throws ServiceException {
		caavoRepository.deleteById(id);		
	}

}
