package com.caavo.boot.document.validator;

import org.hibernate.service.spi.ServiceException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.caavo.boot.document.entity.Details;

public class DetailsValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return Details.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		
		Details details=(Details)arg0;
		if(details.getDetailsType().equalsIgnoreCase("aadhar")) {
			if(String.valueOf(details.getArtifactNumber()).trim().length()!=12)
				throw new ServiceException("Invalid Aadhar Id");
			if(!details.getGender().equalsIgnoreCase("Male") && !details.getGender().equalsIgnoreCase("Female")&&!details.getGender().equalsIgnoreCase("others"))
				throw new ServiceException("Invalid Gender Provided");
		}else if(details.getDetailsType().equalsIgnoreCase("pan")) {
			
		}else if(details.getDetailsType().equalsIgnoreCase("dl")) {
			
		}else if(details.getDetailsType().equalsIgnoreCase("voterid")) {
			
		}	
	}

}
