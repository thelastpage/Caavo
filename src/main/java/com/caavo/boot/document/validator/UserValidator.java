package com.caavo.boot.document.validator;

import org.hibernate.service.spi.ServiceException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.caavo.boot.document.entity.User;

public class UserValidator implements Validator {

	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return User.class.isAssignableFrom(arg0);
	}

	public void validate(Object obj, Errors error)throws ServiceException {
		
		User userDetails=(User)obj;	
		
				String email= userDetails.getMailId();
				if(!email.contains(".") || !email.contains("@"))
					throw new ServiceException("Invalid Email");
				else if(email.split("\\.").length!=2 || email.split("@").length!=2)
					throw new ServiceException("Invalid Email format");
				else if(email.indexOf('.')<email.indexOf('@'))
					throw new ServiceException("Invalid Email format");
				else if(email.split("\\.")[1].length()<2 || email.split("\\.")[1].length()>3)
					throw new ServiceException("Invalid Email");
	}

}
