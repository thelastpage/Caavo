package com.caavo.boot.document;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaavoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaavoApplication.class, args);
	}
}
