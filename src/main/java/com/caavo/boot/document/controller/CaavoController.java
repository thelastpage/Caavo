package com.caavo.boot.document.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import com.caavo.boot.document.dto.DetailsDto;
import com.caavo.boot.document.entity.Details;
import com.caavo.boot.document.entity.User;
import com.caavo.boot.document.exception.ServiceException;
import com.caavo.boot.document.service.DetailsService;
import com.caavo.boot.document.validator.UserValidator;

@RestController
public class CaavoController {
	
	@Autowired
	DetailsService addDetailsService;
	
	@InitBinder("user")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(new UserValidator());
	}
	
	@PostMapping("/addDetails")
	public void addDetails(@RequestBody Details detailsDto,@RequestHeader(value="Authorization") String token) throws ServiceException {
		String arr[]=addDetailsService.getJwtHeader(token).split(" ");
		detailsDto.setUserId(Integer.parseInt(arr[0]));
		addDetailsService.insert(detailsDto);
	}
	
	@GetMapping("/getDetails")
	public List<DetailsDto> getDetails(@RequestHeader(value="Authorization") String token)throws ServiceException {
		String arr[]=addDetailsService.getJwtHeader(token).split(" ");
		List<DetailsDto> list= addDetailsService.fetchDetails(Integer.parseInt(arr[0]));
		return list;
	}
	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestBody User user)throws ServiceException {
		User userDetails= addDetailsService.login(user);
		HttpHeaders header=new HttpHeaders();
		if(null!=userDetails) {
			String jwtHeader=addDetailsService.setJwtHeader(userDetails);			
			header.add("Authorization", jwtHeader);
		}
		return new ResponseEntity<User>(userDetails, header, HttpStatus.OK);
	}
	@PostMapping("/signUp")
	public void signUp(@Valid @RequestBody User user) throws ServiceException{
		addDetailsService.signUp(user);
	}
	
	@GetMapping("/getUserDetails")
	public ResponseEntity<User> getUser(@RequestHeader(value="Authorization") String token)throws ServiceException {
		String arr[]=addDetailsService.getJwtHeader(token).split(" ");
		User user= addDetailsService.getUser(Integer.parseInt(arr[0]));
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@PutMapping("/updateUserDetails")
	public ResponseEntity<User> updateUser(@RequestHeader(value="Authorization") String token,@RequestBody User user)throws ServiceException {
		String arr[]=addDetailsService.getJwtHeader(token).split(" ");
		User userDetails= addDetailsService.updateUser(Integer.parseInt(arr[0]),user);
		return new ResponseEntity<User>(userDetails,HttpStatus.OK);
	}
	@GetMapping("/getLockerDetails")
	public ResponseEntity<List<Details>> getLockerDetails(@RequestHeader(value="Authorization") String token)throws ServiceException {
		String arr[]=addDetailsService.getJwtHeader(token).split(" ");
		List<Details> list= addDetailsService.fetchLockerDetails(Integer.parseInt(arr[0]));
		return new ResponseEntity<List<Details>>(list,HttpStatus.OK);
	}
	@PostMapping("/deleteDetails")
	public ResponseEntity<Integer> deleteDetails(@RequestHeader(value="Authorization") String token,@RequestBody DetailsDto details)throws ServiceException {
		addDetailsService.getJwtHeader(token).split(" ");
		addDetailsService.deleteDetails(details.getId());
		return new ResponseEntity<Integer>(new Integer(1),HttpStatus.OK);
	}
}
