app.controller("userCtrl",function($scope,$localStorage,$location,getUserDetailsService,updateUserDetailsService){

  $scope.user={};
  if($localStorage.login==undefined){
    $location.path('/');
  }

  getUserDetailsService.details().then(function(data){
    $scope.user = data;
    if($scope.user.details!=""){
      $scope.user.records=$scope.user.details.length;
      $scope.user.pending=4-$scope.user.details.length;
    }
  });

  $scope.redirect=function(){
    $location.path("/locker");
  }

  $scope.update=function(){
    updateUserDetailsService.details($scope.user).then(function(data){
      $scope.user = data;
      if($scope.user.details!=""){
        $scope.user.records=$scope.user.details.length;
        $scope.user.pending=4-$scope.user.details.length;
      }
    });
  }
});
