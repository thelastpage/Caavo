app.controller("homeCtrl",function($scope,getDetailsService,deleteDetailsService,$localStorage,$location,DTOptionsBuilder, DTColumnBuilder){

  $scope.user={};
  $scope.datalength=0;
  $scope.unsavedData=0;
  if($localStorage.login==undefined){
    $location.path('/');
  }

  $scope.tableData1 = {
      headerRow: [ 'ID', 'ID Type', 'ID Number', 'Issue Date', 'Actions'],
  };

  getDetailsService.details().then(function(data){
    $scope.tableData1.dataRows = data;
    $scope.datalength=data.length;
    $scope.unsavedData=4-$scope.datalength;
  }).catch(function(data){
    $localStorage.$reset();
    $location.path('/');
  });

  $scope.redirect=function(){
    $location.path("/locker");
  }

  $scope.delete=function(index){
    deleteDetailsService.delete($scope.tableData1.dataRows[index]).then(function(data){
      $scope.tableData1.dataRows.splice(index, 1);
    });
  }

});
