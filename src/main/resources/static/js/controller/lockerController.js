app.controller("lockerCtrl",function($scope,postDetailsService,getLockerDetailsService,$localStorage,$location){

$scope.user={};
if($localStorage.userId!=undefined)
  $scope.user.userId=$localStorage.userId;

if($localStorage.login==undefined){
    $location.path('/');
  }

$scope.selection='Aadhar';
$scope.details={};
$scope.message="";
$scope.changeLayout=function(param){
  $scope.selection=param;
}

getLockerDetailsService.details().then(function(data){
  for(var i=0;i<data.length;i++){
    if(null!=data[i].dob)
      data[i].dob=new Date(data[i].dob);
    if(null!=data[i].issueDate)
      data[i].issueDate=new Date(data[i].issueDate);
    if(data[i].detailsType.toUpperCase()=='aadhar'.toUpperCase())
        $scope.details.aadhar=data[i];
    else if(data[i].detailsType.toUpperCase()=='pan'.toUpperCase())
        $scope.details.pan=data[i];
    else if(data[i].detailsType.toUpperCase()=='voterId'.toUpperCase())
        $scope.details.voterId=data[i];
    else if(data[i].detailsType.toUpperCase()=='dl'.toUpperCase())
        $scope.details.dl=data[i];
  }
});

$scope.addDetails=function(detailsInfo,form){
  var url={};
  if(detailsInfo=='aadhar'){
    $scope.details.aadhar.detailsType="aadhar";
    $scope.details.aadhar.userId=$scope.user.userId;
    url=$scope.details.aadhar;
    $scope.message="Aadhar details updated sucessfully";
  }
  else if(detailsInfo=='dl'){
    $scope.details.dl.detailsType="dl";
    $scope.details.dl.userId=$scope.user.userId;
    url=$scope.details.dl;
    $scope.message="Driving Licence details updated sucessfully";
  }
  else if(detailsInfo=='pan'){
    $scope.details.pan.detailsType="pan";
    $scope.details.pan.userId=$scope.user.userId;
    url=$scope.details.pan;
    $scope.message="Pancard details updated sucessfully";
  }
  else if(detailsInfo=='voterId'){
    $scope.details.voterId.detailsType="voterId";
    $scope.details.voterId.userId=$scope.user.userId;
    url=$scope.details.voterId;
    $scope.message="Voter Id details updated sucessfully";
  }

  if(form.$valid==true){
    postDetailsService.details(url).then(function(data){
      var x = document.getElementById("snackbar");
      // Add the "show" class to DIV
      x.className = "show";
      // After 2 seconds, remove the show class from DIV
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
    });
  }
  else{
      $scope.message="Please re-check the details";
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
  }
}
});
