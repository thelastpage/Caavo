app.controller("indexCtrl",function($scope,$localStorage,$location,$http){
  if($localStorage.login==true){
      $http.defaults.headers.common.Authorization = $localStorage.token;
  }
  $scope.logout=function(){
    $localStorage.$reset();
    $location.path('/');
  }
});
