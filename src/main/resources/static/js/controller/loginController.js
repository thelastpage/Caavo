app.controller('loginCtrl', function($scope, $http, $timeout,$location,$localStorage){

  $scope.message="";

  if($localStorage.login==true){
    $location.path('/home');
  }

  $scope.login=function(){
    if($scope.logging.$valid==true){
      $http.post('/login', $scope.Account).then(function(res){
        if(res.data!="" && res.data.userId!=undefined){
          $localStorage.token=res.headers('Authorization');
          $http.defaults.headers.common.Authorization = $localStorage.token;    
          $scope.snackbar("Login Successful");
          $localStorage.login=true;
          $location.path('/home');
      }
      else{
    	  $scope.snackbar("Invalid Credentials");
          $location.path('/');
        }
      })
    }
    else{
    	$scope.snackbar("Please enter the credentials");
    	$location.path('/');
    }
  }
  $scope.register=function(){
    if($scope.loginForm.$valid==true){
      $http.post('/signUp', $scope.Account).then(function(res){
    	$scope.snackbar("Registration Successful");
        $scope.hideregi();
      })
    }
    else{
      $scope.snackbar("Please fill out the required fields");
    }
  }
  // fade-in &out
  $scope.hiddenreg = true;
  $scope.hidelog = function () {
    $scope.startFadeout = true;
    $scope.startFadein = true;
    $timeout(function(){
      $scope.hiddenlog = true;
      $scope.hiddenreg = false;
    }, 1000);
  };

  $scope.hideregi = function () {
    $scope.startFadeout = false;
    $scope.startFadein = false;
    $timeout(function(){
      $scope.hiddenlog = false;
      $scope.hiddenreg = true;
    }, 100);
  };

  $scope.snackbar=function(message){
	  $scope.message=message;
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
  }
});
