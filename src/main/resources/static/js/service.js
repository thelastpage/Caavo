app.factory("postDetailsService", function($http) {
    return {
        details: function(details) {
            return $http.post("/addDetails",details).then(function(response) {
                return response.data;
            });
        }
    }
});

app.factory("getDetailsService", function($http) {
    return {
        details: function() {
            return $http.get("/getDetails").then(function(response) {
                return response.data;
            });
        }
    }
});

app.factory("getUserDetailsService", function($http) {
    return {
        details: function() {
            return $http.get("/getUserDetails").then(function(response) {
                return response.data;
            });
        }
    }
});

app.factory("updateUserDetailsService", function($http) {
    return {
        details: function(user) {
            return $http.put("/updateUserDetails",user).then(function(response) {
                return response.data;
            });
        }
    }
});

app.factory("getLockerDetailsService", function($http) {
    return {
        details: function() {
            return $http.get("/getLockerDetails").then(function(response) {
                return response.data;
            });
        }
    }
});

app.factory("deleteDetailsService", function($http) {
    return {
        delete: function(data) {
            return $http.post("/deleteDetails",data).then(function(response) {
                return response.data;
            });
        }
    }
});
