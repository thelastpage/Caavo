var app=angular.module("caavo",['ngRoute','ngStorage','datatables']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "html/home.html",
        controller: "loginCtrl"
    })
    .when("/home", {
        templateUrl : "html/dashboard.html",
        controller: "homeCtrl"
    })
    .when("/user", {
        templateUrl : "html/user.html",
        controller: "userCtrl"
    })
    .when("/locker", {
        templateUrl : "html/locker.html",
        controller: "lockerCtrl"
    })
    .when("/profile", {
        templateUrl : "html/dashboard.html",
        controller: function($window,$location){
        $window.location.href="http://mohitagarwal.xyz";
      }
    })
    .otherwise({
      redirectTo: "/"
    });
});
