app.directive("sidebar",function(){
  return{
    templateUrl:"html/sidebar.html",
    link : function (scope) {
      scope.routes=[
          { path: '#!home', title: 'Dashboard',  icon: 'ti-panel', class: '' },
          { path: '#!user', title: 'User Profile',  icon:'ti-user', class: '' },
          { path: '#!locker', title: 'Locker',  icon:'ti-view-list-alt', class: '' },
          { path: '#!profile', title: 'Profile',  icon:'ti-bell', class: '' }
      ];
    }
  }
});

app.directive("navbar",function(){
  return{
    templateUrl:"html/navbar.html"
  }
});

app.directive("aadhar",function(){
  return{
    templateUrl:"html/aadhar.html"
  }
});

app.directive("dl",function(){
  return{
    templateUrl:"html/dl.html"
  }
});
app.directive("pancard",function(){
  return{
    templateUrl:"html/pancard.html"
  }
});

app.directive("voterid",function(){
  return{
    templateUrl:"html/voterid.html"
  }
});
